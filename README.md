# Network Time Security (NTS) client in Go

This is a small
[NTS](https://datatracker.ietf.org/doc/draft-ietf-ntp-using-nts-for-ntp/)
client in Go to query for authenticated time. Most of the work is done
by:

- [ntske](https://gitlab.com/hacklunch/ntske) NTS Key Exchange Go library
- [ntp](https://gitlab.com/hacklunch/ntp) NTP/NTS Go library

# Building

With Go 1.11 or later installed, build ntsclient like so:

    make

# Usage

    ntsclient -config /etc/ntsclient.toml -set

This will read a configuration file and actually attempt to set system
time. Using `-set` means ntsclient will have to run as root (on many
systems) or be awarded some capability or similar.

On Linux, set the CAP_SYS_TIME capability like so:

    sudo setcap CAP_SYS_TIME+ep ./ntsclient

See the `ntsclient.toml` for configuration suggestions.

# Packages

ntsclient has been packaged for the following systems:

## Arch Linux User Repository (AUR)

The package builds directly from the Git repository and is named:
[ntsclient-git](https://aur.archlinux.org/packages/ntsclient-git/).
It can be installed manually as per the
[official instructions](https://wiki.archlinux.org/index.php/Arch_User_Repository#Installing_packages).
Or using an AUR helper, such as `yay`:

    yay -S ntsclient-git

## RHEL/CentOS/Fedora RPM

For now an RPM is built using Pipelines, so look for the Artifacts download icon in the right hand side of the [pipelines page](https://gitlab.com/hacklunch/ntsclient/pipelines).
