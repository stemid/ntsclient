// +build ignore

package main

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"regexp"
	"strconv"
	"strings"
)

const (
	versionFile = "version.go"
)

func main() {
	if len(os.Args) != 2 {
		fmt.Printf("commands: major, minor, or patch\n")
		os.Exit(2)
	}

	if err := ensureBranch("master"); err != nil {
		fmt.Printf("%v\n", err)
		os.Exit(1)
	}

	if err := ensureCleanRepo(); err != nil {
		fmt.Printf("%v\n", err)
		os.Exit(1)
	}

	current := version

	fmt.Printf("current: %s\n", current)

	ss := strings.Split(strings.TrimPrefix(current, "v"), ".")
	if len(ss) != 3 {
		panic("doesn't have 3 parts")
	}

	mmp := make([]int, len(ss))
	for i, s := range ss {
		var err error
		mmp[i], err = strconv.Atoi(s)
		if err != nil {
			panic("part not int")
		}
	}

	switch os.Args[1] {
	case "major":
		mmp[0] += 1
		mmp[1] = 0
		mmp[2] = 0
	case "minor":
		mmp[1] += 1
		mmp[2] = 0
	case "patch":
		mmp[2] += 1
	default:
		fmt.Printf("bad bump command\n")
		os.Exit(2)
	}

	bumped := fmt.Sprintf("v%d.%d.%d", mmp[0], mmp[1], mmp[2])
	fmt.Printf(" bumped: %s\n\n", bumped)

	if err := doBumpVersion(current, bumped); err != nil {
		fmt.Printf("bump failed: %v\n", err)
		os.Exit(1)
	}

	if err := doCommitAndTag(bumped); err != nil {
		fmt.Printf("tag failed: %v\n", err)
		os.Exit(1)
	}

	fmt.Printf(`
	Version in %s was bumped, committed and tagged. You may now do:
	    git push origin
	    git push origin %s
	`, versionFile, bumped)
}

func ensureBranch(branch string) error {
	err, output := run("git symbolic-ref HEAD")
	if err != nil {
		return fmt.Errorf("Failed to determine current branch: %v", err)
	}
	if strings.TrimPrefix(strings.TrimSpace(output), "refs/heads/") != branch {
		return fmt.Errorf("Current branch is not %s: %s", branch, output)
	}
	return nil
}

func ensureCleanRepo() error {
	_, _ = run("git update-index -q --ignore-submodules --refresh")
	err, _ := run("git diff-files --quiet --ignore-submodules --")
	if err != nil {
		return fmt.Errorf("Working tree has changes. Refusing to bump version.")
	}
	err, _ = run("git diff-index --cached --quiet HEAD --ignore-submodules --")
	if err != nil {
		return fmt.Errorf("Repo index has staged changes. Refusing to bump version.")
	}
	return nil
}

func doBumpVersion(current string, bumped string) error {
	old, err := ioutil.ReadFile(versionFile)
	if err != nil {
		return err
	}
	re := regexp.MustCompile(fmt.Sprintf(`(\s+version\s+=\s+")%s(")`, regexp.QuoteMeta(current)))
	new := re.ReplaceAll(old, []byte(fmt.Sprintf("${1}%s${2}", bumped)))
	if bytes.Equal(old, new) {
		return fmt.Errorf("no change?!")
	}
	err = ioutil.WriteFile("version.go", []byte(new), 0)
	return err
}

func doCommitAndTag(bumped string) error {
	err, output := run(fmt.Sprintf("git add %s", versionFile))
	fmt.Print(output)
	if err != nil {
		return err
	}
	err, output = run(fmt.Sprintf("git commit -m Bump to %s", bumped))
	fmt.Print(output)
	if err != nil {
		return err
	}
	err, output = run(fmt.Sprintf("git tag -a %s -m %s", bumped, bumped))
	fmt.Print(output)
	if err != nil {
		return err
	}
	return nil
}

// Run executes a cmdline and returns combined stdout and stderr.
//
// Cmdline special in that it is first split on space (0x20) into program name
// and arguments. Then all non-breaking space (0xA0) are replaced by space.
// That way, we can easily run a cmdline with arguments containing spaces.
func run(cmdline string) (error, string) {
	s := strings.Split(cmdline, " ")
	for n, _ := range s {
		s[n] = strings.ReplaceAll(s[n], " ", " ")
	}
	cmd := exec.Command(s[0], s[1:]...)
	stdboth, err := cmd.CombinedOutput()
	return err, string(stdboth)
}
