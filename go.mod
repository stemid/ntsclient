module gitlab.com/hacklunch/ntsclient

require (
	github.com/BurntSushi/toml v0.3.1
	gitlab.com/hacklunch/ntp v0.2.1-0.20190819112626-2096ac2a0a1a
	gitlab.com/hacklunch/ntske v0.0.0-20191103142658-ba9e7624cb3e
	golang.org/x/net v0.0.0-20191101175033-0deb6923b6d9 // indirect
	golang.org/x/sys v0.0.0-20191029155521-f43be2a4598c // indirect
)

go 1.12
